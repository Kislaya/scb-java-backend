package com.example.azureAD;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping("/")
public class Controller {

//    @GetMapping
////    @ModelAttribute
//    public Authentication index(Model model, Authentication user) {
//        model.addAttribute("user", user);
//        return user;
//    }

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("test")
    public String callDemoBackstage(){
        String url = "http://localhost:7007/api/demo/test";
        ResponseEntity<String> response = restTemplate.getForEntity(url, String.class);
        String responseBody = response.getBody();
        return responseBody;
    }

    @GetMapping("login/oauth2/code/azure-dev")
    @ResponseBody
    public String redirect() {
        return "Successfully logged in!";
    }
}
